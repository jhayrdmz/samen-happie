import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from './../../../providers/api/api';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public email: string;
  public password: string;
  public responseData;

  // LOGIN_URL = "http://samenhappie.diodevs.com/yipp/";
  public loginForm : FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http, 
    private nativeStorage: NativeStorage,
    private translateService: TranslateService,
    public alertCtrl: AlertController,
    public api: ApiProvider,
    private formBuilder: FormBuilder
    ) {
    this.translateService.onLangChange.subscribe((event) => {
      // do something
      console.log(event);
    });

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  



  gotoPage(page, pageType?) {
    if (!!page) {
      if(!!pageType) {
        if(pageType.toLowerCase() === 'root') {
          this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
        }
      } else {
        this.navCtrl.push(page,{},{animate:false});
      }
    }
    page = null;
  }

  showAlert(title,message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }


  signin(page, pageType?){
    
    this.api.login(this.email,this.password).then(
      data => {      
        this.responseData = data;
        console.log(this.responseData);
        if(this.responseData.status == "OK"){
          // alert('yeah here you go!');
          this.nativeStorage.setItem('login_result', this.responseData.result);
          this.navCtrl.push(page,{},{animate:false});
        }
      
      
    },
      err => {
        console.log(err);
        if (err.status == 400) {          
          // console.log(err.error.result.message)
          this.showAlert('Oops!',err.error.result.message);
        } else if(err.status == 403){
          // alert(error.result.error)
          this.showAlert('Oops!',err.error.result.error);
        }
      }
    );    
  }

  

}
