import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreloginPage } from './prelogin';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    PreloginPage,
  ],
  imports: [
    IonicPageModule.forChild(PreloginPage),
    TranslateModule.forChild()
  ],
})
export class PreloginPageModule {}
