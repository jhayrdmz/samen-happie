import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationParentPage } from './registration-parent';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegistrationParentPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationParentPage),
    TranslateModule.forChild()
  ],
})
export class RegistrationParentPageModule {}
