import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { ApiProvider } from './../../../providers/api/api';


/**
 * Generated class for the RegistrationParentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration-parent',
  templateUrl: 'registration-parent.html',
})
export class RegistrationParentPage {

  public registerParentForm : FormGroup;
  public email: string;
  public password: string;
  public name: string;
  public parent_type: string;
  public service_id: number;
  public regData:{};
  public responseData;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public api: ApiProvider,
    public alertCtrl: AlertController,
    private nativeStorage: NativeStorage,
    ) {

    this.parent_type = 'mother';
    this.service_id = 2;

    this.registerParentForm = this.formBuilder.group({
      name: ['', Validators.required],
      // email: ['', Validators.required],
      email: new FormControl('', Validators.compose([
         Validators.required,
         Validators.pattern("[^ @]*@[^ @]*")
       ])),
      password: new FormControl('', Validators.compose([
         Validators.required,
         Validators.minLength(8)
       ])),
      // password: ['', Validators.required,Validators.minLength(5)]
    });
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationParentPage');
  }

  // gotoPage(page, pageType?) {
  //   if (!!page) {
  //     if(!!pageType) {
  //       if(pageType.toLowerCase() === 'root') {
  //         this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
  //       }
  //     } else {
  //       this.navCtrl.push(page,{},{animate:false});
  //     }
  //   }
  //   page = null;
  // }

  tabSelect(tabType){
    this.parent_type = tabType;
  }
  showAlert(title,message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  register(page, pageType?){
    this.regData = {
      'email': this.email,
      'name': this.name,
      'parent_type': this.parent_type,
      'password': this.password,
      'service_id': this.service_id
    }
    console.log(this.regData)
    this.api.parentRegister(this.regData).then(
      data => {      
        this.responseData = data;
        console.log(this.responseData);
        if(this.responseData.status == "OK"){         
          // // alert('yeah here you go!');
          this.nativeStorage.setItem('parent_data', {email:this.email,name: this.name,parent_type: this.parent_type});
          this.nativeStorage.setItem('signup_parent', this.responseData.result);
          this.navCtrl.push(page,{},{animate:false});
        }     
      
    },
      err => {
        console.log(err);
        console.log(err.status);
        if(err.status == 400){
          console.log(err.error.result.message)
          this.showAlert('Oops!',err.error.result.message);
        } else{
          this.showAlert('Oops!','Something went wrong.');
        }        
      }
    );  
  }

}
