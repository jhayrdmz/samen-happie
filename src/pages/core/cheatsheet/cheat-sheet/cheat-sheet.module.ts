import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheatSheetPage } from './cheat-sheet';

@NgModule({
  declarations: [
    CheatSheetPage,
  ],
  imports: [
    IonicPageModule.forChild(CheatSheetPage),
  ],
})
export class CheatSheetPageModule {}
