import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChallengeSetDonePage } from './challenge-set-done';

@NgModule({
  declarations: [
    ChallengeSetDonePage,
  ],
  imports: [
    IonicPageModule.forChild(ChallengeSetDonePage),
  ],
})
export class ChallengeSetDonePageModule {}
