import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChancePage } from './chance';

@NgModule({
  declarations: [
    ChancePage,
  ],
  imports: [
    IonicPageModule.forChild(ChancePage),
  ],
})
export class ChancePageModule {}
