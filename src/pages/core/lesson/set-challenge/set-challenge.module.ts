import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetChallengePage } from './set-challenge';

@NgModule({
  declarations: [
    SetChallengePage,
  ],
  imports: [
    IonicPageModule.forChild(SetChallengePage),
  ],
})
export class SetChallengePageModule {}
