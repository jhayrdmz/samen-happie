import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourStartPage } from './tour-start';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TourStartPage,
  ],
  imports: [
    IonicPageModule.forChild(TourStartPage),
    TranslateModule.forChild()
  ],
})
export class TourStartPageModule {}
