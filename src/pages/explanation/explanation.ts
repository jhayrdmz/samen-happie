import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the ExplanationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-explanation',
  templateUrl: 'explanation.html',
})
export class ExplanationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private nativeStorage: NativeStorage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExplanationPage');
  }

  gotoPage(page, pageType?) {
    console.log('SKIP')
    this.nativeStorage.setItem('skip', true)
    .then(
      () => console.log('Stored item!'),
      error => console.error('Error storing item', error)
    );  

    if (!!page) {
      if(!!pageType) {
        if(pageType.toLowerCase() === 'root') {
          this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
        }
      } else {
        this.navCtrl.push(page,{},{animate:false});
      }
    }
    page = null;
  }

}
