import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';

import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

	private apiURL: string = '/api/users';
  	public authToken: string;
  	public URL = "http://samenhappie.diodevs.com/yipp/";
  	contentHeader: Headers = new Headers({"Content-Type": "application/json"});

	constructor(
		public http: HttpClient,
		private nativeStorage: NativeStorage,
		) {
	   console.log('Hello ApiProvider Provider');
	}

	

	public login(email, password) {
	    console.log(this.getUrl('login'));
	    let data:any = {};
	    data.email = email; //'dev.cyrusjayson@gmail.com';
	    data.password = password;//'admin123';
	    console.log(data);
	    
	    return new Promise((resolve, reject) => {
		    this.http.post(this.URL+'login', data, {})
		    .subscribe(
		      res => {
		        console.log(res);
		        resolve(res)		        
		      }, (err) => {
		        console.log(err);
		        reject(err)		        
		      }
		    );
		});	      
	}

	public parentRegister(parentData) {
	    console.log(this.getUrl('parentregister'));
	    
	    
	    return new Promise((resolve, reject) => {
		    this.http.post(this.URL+'parentregister', parentData, {})
		    .subscribe(
		      res => {
		        // console.log(res);
		        resolve(res)		        
		      }, (err) => {
		        // console.log(err);
		        reject(err)		        
		      }
		    );
		});	  
	}

	public childRegister(parentData) {
	    console.log(this.getUrl('childRegister'));	    
	    
	    return new Promise((resolve, reject) => {
		    this.http.post(this.URL+'childregister', parentData, {})
		    .subscribe(
		      res => {
		        // console.log(res);
		        resolve(res)		        
		      }, (err) => {
		        // console.log(err);
		        reject(err)		        
		      }
		    );
		});	  
	}

	public getLocalStorage(storageName){	

	    return Observable.create(observer => {
	    	this.nativeStorage.getItem(storageName)
		    .then(
		      data => {
		        // console.log(data)
		        observer.next(data);
		      },
		      error => {
		      	// console.error(error)
		      	observer.next(error);
		      }
		    );	    	
	    }
	    );
	}

	private getUrl(endPoint) {
	  	return `${this.apiURL}/${endPoint}`;
	}

}
