import { HttpClient } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { ApiProvider } from './../api/api';
import { NativeStorage } from '@ionic-native/native-storage';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

	private _authenticated: boolean;

  	@Output() onAuthStateChange = new EventEmitter();

  	constructor(
  		public http: HttpClient, 
  		private apiProvider: ApiProvider,
  		private nativeStorage: NativeStorage
  		) {
    	console.log('Hello AuthProvider Provider');
  	}

  	public signin(email, password) {
    	return this.apiProvider.login(email, password);
      //.do(res => this.saveToken(res.token));
  	}
  	public saveToken(token) {
	    this.authenticated = !!token;
	    this.apiProvider.authToken = token;
	    !!token ? this.nativeStorage.setItem('token', token) : this.nativeStorage.remove('token');
	 }
	public get authenticated(): boolean {
	   return this._authenticated;
	 }

	 public set authenticated(_authenticated) {
	    if (this._authenticated !== _authenticated) this.onAuthStateChange.emit(_authenticated);
	    this._authenticated = _authenticated;
	 }

}
